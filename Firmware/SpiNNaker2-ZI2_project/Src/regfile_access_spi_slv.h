/*
 * regfile_access_spi_slv.h
 *
 *  Created on: 11.07.2019
 *      Author: stolba
 */

#ifndef JIB1_REGFILE_ACCESS_SPI_SLV_H_
#define JIB1_REGFILE_ACCESS_SPI_SLV_H_

#include "regfile_access_noc_mmap.h"

// RA_SPI_SLV_EN       0x00000080
// RA_SPI_SLV_CLK_DIV  0x00000084
// RA_SPI_CONFIG_IN    0x00000088
// RA_SPI_CONFIG_OUT   0x0000008c


uint8_t rf_spi_slv_en(uint8_t enable){
    return write_top_rf (RA_SPI_SLV_EN, enable);
}


uint8_t rf_spi_set_clk_div(uint8_t clk_div){
    uint32_t wdata;
    wdata = (0x3<<16) | (clk_div & 0x3);        // write mask and clk div value
    return  write_top_rf (RA_SPI_SLV_CLK_DIV, wdata);
}

uint8_t rf_spi_set_config(	uint8_t     source_id,
							uint8_t     spi_auto_mode,
							uint8_t     miso_regs,
							uint8_t     mmap_segment,
							uint8_t     spi_tx_edge_falling,
							uint8_t     spi_rx_edge_rising,
							uint8_t     miso_z,
							uint8_t     miso_tx_z,
							uint8_t		tx_wait_cycle,
							uint8_t     spi_type,
							uint8_t     dqspi_cmd,
							uint8_t     dqspi_wait,
							uint8_t     dqspi_head,
							uint8_t     dqspi_addr,
							uint8_t     dqspi_data,
							uint8_t     rising_edge_shift
							){
    // config vector in
    // [7:0]   - source ID of the SPI slave        default {3'b010, 3'b000,2'b10}, 0x42
    // [9:8]   - mmap_global_segment               default 2'b00
    // [12]    - automatic SPI mode detection      default 1'b0
    // [14]    - spi_tx_edge_falling               default 1'b0
    // [15]    - spi_rx_edge_rising                default 1'b0
    // [17:16] - MISO register stages              default 2'b00
    // [18]    - MISO drive z                      default 1'b1
    // [19]    - MISO drive z, when tx active      default 1'b0 (when ~nss)
    // [20]    - enable wait cycle between master TX and slave TX switching
    // [22:21] - SPI type; 2'b00 classic; 2'b01 dual; 2'b10 quad
    // [23]    - enable DSPI / QSPI for CMD  cycle
    // [24]    - enable DSPI / QSPI for WAIT cycle
    // [25]    - enable DSPI / QSPI for HEAD cycle
    // [26]    - enable DSPI / QSPI for ADDR cycle
    // [27]    - enable DSPI / QSPI for DATA cycle
    // [28]    - rising edge comes from previous falling edge

    uint32_t w_data = 0;

    w_data = 	((uint32_t)(rising_edge_shift)        << 28) |
    			((uint32_t)(dqspi_data)               << 27) |
				((uint32_t)(dqspi_addr)               << 26) |
				((uint32_t)(dqspi_head)               << 25) |
				((uint32_t)(dqspi_wait)               << 24) |
				((uint32_t)(dqspi_cmd)                << 23) |
				((uint32_t)(spi_type & 0x3)           << 21) |
				((uint32_t)(tx_wait_cycle)            << 20) |
    		    ((uint32_t)(miso_tx_z)                << 19) |
                ((uint32_t)(miso_z)                   << 18) |
                ((uint32_t)(miso_regs & 0x3)          << 16) |
                ((uint32_t)(spi_rx_edge_rising)       << 15) |
                ((uint32_t)(spi_tx_edge_falling)      << 14) |
                ((uint32_t)(spi_auto_mode & 0x1)      << 12) |
                ((uint32_t)(mmap_segment & 0x3)       <<  8) |
                ((uint32_t)(source_id & 0xff)         <<  0) ;

    return write_top_rf (RA_SPI_CONFIG_IN, w_data);
}

uint8_t rf_spi_slv_get_status(uint8_t  *  packet_tx_stall,
                              uint16_t *  packet_rx_pld_type,
                              uint8_t  *  packet_rx_ready,
							  uint8_t  *  cpol,
							  uint8_t  *  cpol_inv,
							  uint8_t  *  f_error,
							  uint8_t  *  f_cnt_high,
							  uint8_t  *  f_cnt_low){
    uint32_t rdata;
    read_top_rf (RA_SPI_CONFIG_OUT, &rdata);

    *f_cnt_low           = ((rdata >> 0)  & 0xff);
    *f_cnt_high          = ((rdata >> 8)  & 0xff);
    *f_error             = ((rdata >> 16) & 0x01);
    *cpol                = ((rdata >> 17) & 0x01);
    *cpol_inv            = ((rdata >> 18) & 0x01);

    *packet_rx_ready     = ((rdata >> 24) & 0x01);
    *packet_rx_pld_type  = ((rdata >> 25) & 0x07);
    *packet_tx_stall     = ((rdata >> 28) & 0x01);

    return 1;
}

uint8_t rf_spi_slv_get_mst_freq(uint32_t * frequency_meg){
    uint32_t rdata;
    read_top_rf (RA_SPI_CONFIG_OUT, &rdata);

    uint8_t f_cnt_low   = ((rdata >> 0)  & 0xff) + 1; // low time  = (cnt+1) * 10ns
    uint8_t f_cnt_high  = ((rdata >> 8)  & 0xff) + 1; // high time = (cnt+1) * 10ns

    uint32_t t_periode  = (f_cnt_low + f_cnt_high)*10;

    *frequency_meg = 1000 / t_periode;

    return 1;
}



#endif /* JIB1_REGFILE_ACCESS_SPI_SLV_H_ */
