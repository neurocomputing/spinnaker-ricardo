/*
 * regfile_access_top.h
 *
 *  Created on: Jul 11, 2019
 *      Author: stolba
 */

#ifndef JIB1_REGFILE_ACCESS_TOP_H_
#define JIB1_REGFILE_ACCESS_TOP_H_

enum gpio_config {
    GPIO_CFG_INACTIVE        = 0x00,

    GPIO_CFG_CUART_TX        = 0x01,
    GPIO_CFG_CUART_RX        = 0x02,

    GPIO_CFG_UART0_TX        = 0x03,
    GPIO_CFG_UART0_RX        = 0x04,
    GPIO_CFG_UART0_CTS       = 0x05,
    GPIO_CFG_UART0_RTR       = 0x06,

    GPIO_CFG_UART1_TX        = 0x07,
    GPIO_CFG_UART1_RX        = 0x08,
    GPIO_CFG_UART1_CTS       = 0x09,
    GPIO_CFG_UART1_RTR       = 0x0a,

    GPIO_CFG_SPI_INTR        = 0x0b,
    GPIO_CFG_SPI_MISO        = 0x0c,
    GPIO_CFG_SPI_MOSI        = 0x0d,
    GPIO_CFG_SPI_NSS         = 0x0e,
    GPIO_CFG_SPI_SCLK        = 0x0f,

    GPIO_CFG_GPIOREG_0       = 0x10,
    GPIO_CFG_GPIOREG_1       = 0x11,
    GPIO_CFG_GPIOREG_2       = 0x12,
    GPIO_CFG_GPIOREG_3       = 0x13,
    GPIO_CFG_GPIOREG_4       = 0x14,
    GPIO_CFG_GPIOREG_5       = 0x15,
    GPIO_CFG_GPIOREG_6       = 0x16,
    GPIO_CFG_GPIOREG_7       = 0x17,
    GPIO_CFG_GPIOREG_8       = 0x18,
    GPIO_CFG_GPIOREG_9       = 0x19,
    GPIO_CFG_GPIOREG_A       = 0x1a,
    GPIO_CFG_GPIOREG_B       = 0x1b,
    GPIO_CFG_GPIOREG_C       = 0x1c,
    GPIO_CFG_GPIOREG_D       = 0x1d,
    GPIO_CFG_GPIOREG_E       = 0x1e,
    GPIO_CFG_GPIOREG_F       = 0x1f,

    GPIO_CFG_ARM_JTAG1_TCK   = 0x20,
    GPIO_CFG_ARM_JTAG1_TMS   = 0x21,
    GPIO_CFG_ARM_JTAG1_TRSTN = 0x22,
    GPIO_CFG_ARM_JTAG1_TDI   = 0x23,
    GPIO_CFG_ARM_JTAG1_TDO   = 0x24,

    GPIO_CFG_ARM_JTAG2_TCK   = 0x25,
    GPIO_CFG_ARM_JTAG2_TMS   = 0x26,
    GPIO_CFG_ARM_JTAG2_TRSTN = 0x27,
    GPIO_CFG_ARM_JTAG2_TDI   = 0x28,
    GPIO_CFG_ARM_JTAG2_TDO   = 0x29,

    GPIO_CFG_ARM_DBG_RST     = 0x2a,

    GPIO_CFG_FBIST_MEAS      = 0x2b,

    GPIO_CFG_PLL_C0_PFD      = 0x2c,
    GPIO_CFG_PLL_C2C_0_PFD   = 0x2d,
    GPIO_CFG_PLL_C2C_1_PFD   = 0x2e,

    GPIO_CFG_PWM0            = 0x2f,
    GPIO_CFG_PWM1            = 0x30,

    GPIO_CFG_OSPI_CLK        = 0x31,
    GPIO_CFG_OSPI_NSS0       = 0x32,
    GPIO_CFG_OSPI_NSS1       = 0x33,
    GPIO_CFG_OSPI_NSS2       = 0x34,
    GPIO_CFG_OSPI_NSS3       = 0x35,

    GPIO_CFG_OSPI_IO0        = 0x36,
    GPIO_CFG_OSPI_IO1        = 0x37,
    GPIO_CFG_OSPI_IO2        = 0x38,
    GPIO_CFG_OSPI_IO3        = 0x39,
    GPIO_CFG_OSPI_IO4        = 0x3a,
    GPIO_CFG_OSPI_IO5        = 0x3b,
    GPIO_CFG_OSPI_IO6        = 0x3c,
    GPIO_CFG_OSPI_IO7        = 0x3d,

};

enum gpio_padnum {
    GPIO_PAD_GPIO0 = 0,
    GPIO_PAD_GPIO1 = 1,
    GPIO_PAD_GPIO2 = 2,
    GPIO_PAD_GPIO3 = 3,

    GPIO_PAD_TCK   = 4,
    GPIO_PAD_TMS   = 5,
    GPIO_PAD_TDI   = 6,
    GPIO_PAD_TDO   = 7
};

uint8_t rf_top_jtag_as_gpio (uint8_t activate)
{
    uint8_t retval = 1;

    retval &= write_top_rf (0x1c, (activate ? 0x5a5a5a5a : 0));

    return retval;
}

uint8_t rf_top_gpio_dft_enable (uint8_t activate)
{
    uint8_t retval = 1;

    retval &= write_top_rf (0x1c, (activate ? 0x3c3c3c3c : 0xc3c3c3c3));

    return retval;
}

uint8_t rf_top_gpio_config (uint8_t gpio_num, enum gpio_config gpio_config, uint32_t ds)
{
    uint8_t retval = 1;

    retval &= write_top_rf (0x20 + 4*gpio_num, ds | gpio_config);

    return retval;
}


uint8_t rf_top_clksystick_config (uint8_t clk_core_en, uint8_t systick_en, uint16_t systick_div)
{
    uint32_t val = 0;

    val |= (systick_div << 16);
    val |= (systick_en ? 1 : 0) << 8;
    val |= (clk_core_en ? 1 : 0);

    return write_top_rf (0x18, val);
}

uint8_t rf_top_dft_config (uint32_t value)
{
    return write_top_rf (0x90, value);
}

uint8_t rf_top_dft_pattern (uint32_t addr, uint32_t size)
{
    uint8_t result = 1;
    result &= write_top_rf (0x98, addr);
    result &= write_top_rf (0x9c, size);

    return result;
}

uint8_t rf_top_dft_status (uint32_t *value)
{
    return read_top_rf (0x94, value);
}

uint8_t rf_top_get_chipid (uint32_t *value)
{
    // get active chip ID
    return read_top_rf (0x78, value);
}

uint8_t rf_top_set_chipid (uint32_t value)
{
    return write_top_rf (0x78, value);
}

uint8_t rf_top_abb_config (uint32_t ctrl0, uint32_t ctrl1, uint32_t ctrl2, uint32_t filter0, uint32_t filter1, uint32_t pwm)
{
    uint8_t result = 1;

    result &= write_top_rf (0xa0, ctrl0);
    result &= write_top_rf (0xa4, ctrl1);
    result &= write_top_rf (0xa8, ctrl2);
    result &= write_top_rf (0xac, filter0);
    result &= write_top_rf (0xb0, filter1);
    result &= write_top_rf (0xb4, pwm);

    return result;
}

uint8_t rf_top_abb_status (uint32_t *status0, uint32_t *status1)
{
    uint8_t result = 1;

    result &= write_top_rf (0xb8, 0x01);
    result &= read_top_rf  (0xb8, status0);
    result &= read_top_rf  (0xbc, status1);

    return result;
}

uint8_t rf_lockreg_write (int regnum, uint8_t identifier, uint8_t lock)
{
    uint32_t addr = 0x80 + 4*(regnum & 0xf);
    uint32_t val = 0;

    if (lock) val |= (1<<8);

    val |= identifier;

    return write_jibcore_rf (addr, val);
}

uint8_t rf_lockreg_read (int regnum, uint8_t *identifier, uint8_t *locked)
{
    uint32_t addr = 0x80 + 4*(regnum & 0xf);
    uint32_t val = 0;

    uint8_t result = read_jibcore_rf (addr, &val);

    *identifier = val & 0xff;
    *locked     = ((val & 0x100) != 0);

    return result;
}


uint8_t rf_rterm_clock  (uint8_t divval)
{
    uint32_t addr = 0x70;
    uint32_t val  = divval;
    uint8_t result = write_jibcore_rf (addr, val);
    return result;
}

uint8_t rf_rterm_config (uint8_t en, uint8_t ovr_lvds, uint8_t ovr_c2c, uint8_t val_lvds, uint8_t val_c2c)
{
    uint32_t addr = 0x74;
    uint32_t val  = 0;

    val |= (en       ? 1 : 0) << 0;
    val |= (ovr_lvds ? 1 : 0) << 1;
    val |= (ovr_c2c  ? 1 : 0) << 2;
    val |= (ovr_c2c  ? 1 : 0) << 2;
    val |= (val_c2c  & 0xf)   << 16;
    val |= (val_lvds & 0xf)   << 24;

    uint8_t result = write_jibcore_rf (addr, val);
    return result;
}

uint8_t rf_rterm_status (uint8_t *done, uint8_t *val_lvds, uint8_t *val_c2c, uint16_t *val_c2c_thermo)
{
    uint32_t addr = 0x78;
    uint32_t val  = 0;
    uint8_t result = read_jibcore_rf (addr, &val);

    *done = ((val & 1) == 1 ? 1 : 0);

    *val_c2c_thermo = (val >> 1)  & 0x7fff;
    *val_c2c        = (val >> 16) & 0xf;
    *val_lvds       = (val >> 24) & 0xf;

    return result;
}

uint8_t rf_vbias_config (uint8_t en, uint8_t en_vbias, uint8_t startup, uint8_t trim_bias, uint8_t trim_curv, uint8_t trim_vbg)
{
    uint32_t addr = 0x7c;
    uint32_t val  = 0;

    val |= (en        ? 1 : 0) << 0;
    val |= (en_vbias  ? 1 : 0) << 1;
    val |= (startup   ? 1 : 0) << 2;
    val |= (trim_bias & 0xf)   << 8;
    val |= (trim_curv & 0xff)  << 16;
    val |= (trim_vbg  & 0xff)  << 24;

    uint8_t result = write_jibcore_rf (addr, val);
    return result;
}

uint8_t rf_vbias_status (uint8_t *valid_n)
{
    uint32_t addr = 0x7c;
    uint32_t val  = 0;

    uint8_t result = read_jibcore_rf (addr, &val);

    *valid_n = (((val >> 3) & 1) == 1);

    return result;
}


#endif /* JIB1_REGFILE_ACCESS_TOP_H_ */
