/*
 * jib1_defines.h
 *
 *  Created on: Jul 5, 2019
 *      Author: stolba
 */

#ifndef JIB1_DEFINES_H_
#define JIB1_DEFINES_H_


// Port def


#define SPI_DEFAULT_TIMEOUT 10
// SPI slave commands
///////////////////////////////////////////////////////////////////////////////
// SPI slave command set
#define CMD_J_STATUS         0xC0 // B_RX_CMD, B_TX_S0

#define CMD_J_WRITE_0        0xAC // write only header and addr               // B_RX_CMD, B_RX_H3,...,B_RX_H0, B_RX_A3,...,B_RX_A0
#define CMD_J_WRITE_32       0xAD // write header address and 32bit data      // B_RX_CMD, B_RX_H3,...,B_RX_H0, B_RX_A3,...,B_RX_A0, B_RX_D3,...,B_RX_D0
#define CMD_J_WRITE_64       0xAE // write header address and 64bit data      // B_RX_CMD, B_RX_H3,...,B_RX_H0, B_RX_A3,...,B_RX_A0, B_RX_D7,...,B_RX_D0
#define CMD_J_WRITE_128      0xAF // write header address and 128bit data     // B_RX_CMD, B_RX_H3,...,B_RX_H0, B_RX_A3,...,B_RX_A0, B_RX_D15,...,B_RX_D0
#define CMD_J_WRITE_MMAP_R32 0xA4 // memory mapped read request  32bit data   // B_RX_CMD, B_RX_A3,...,B_RX_A0
#define CMD_J_WRITE_MMAP_W32 0xA5 // memory mapped write request 32bit data   // B_RX_CMD, B_RX_A3,...,B_RX_A0, B_RX_D3,...,B_RX_D0

#define CMD_J_READ_D_32      0x91 // read only data 32bit data                // B_RX_CMD, B_TX_D3,...,B_TX_D0
#define CMD_J_READ_D_64      0x92 // read only data 64bit data                // B_RX_CMD, B_TX_D7,...,B_TX_D0
#define CMD_J_READ_D_128     0x93 // read only data 128bit data               // B_RX_CMD, B_TX_D15,...,B_TX_D0
#define CMD_J_READ_AD_32     0x95 // read addr and data 32bit data            // B_RX_CMD, B_TX_A3,...,B_TX_A0, B_TX_D3,...,B_TX_D0
#define CMD_J_READ_AD_64     0x96 // read addr and data 64bit data            // B_RX_CMD, B_TX_A3,...,B_TX_A0, B_TX_D7,...,B_TX_D0
#define CMD_J_READ_AD_128    0x97 // read addr and data 128bit data           // B_RX_CMD, B_TX_A3,...,B_TX_A0, B_TX_D15,...,B_TX_D0
#define CMD_J_READ_HAD_32    0x9D // read header, addr and 32bit data         // B_RX_CMD, B_TX_H3,...,B_TX_H0, B_TX_A3,...,B_TX_A0, B_TX_D3,...,B_TX_D0
#define CMD_J_READ_HAD_64    0x9E // read header, addr and 64bit data         // B_RX_CMD, B_TX_H3,...,B_TX_H0, B_TX_A3,...,B_TX_A0, B_TX_D7,...,B_TX_D0
#define CMD_J_READ_HAD_128   0x9F // read header, addr and 128bit data        // B_RX_CMD, B_TX_H3,...,B_TX_H0, B_TX_A3,...,B_TX_A0, B_TX_D15,...,B_TX_D0

///////////////////////////////////////////////////////////////////////////////
#define RA_SPI_SLV_EN         0x00000080
#define RA_SPI_SLV_CLK_DIV    0x00000084
#define RA_SPI_CONFIG_IN      0x00000088
#define RA_SPI_CONFIG_OUT     0x0000008c

#define RA_DNOC_ROUTER_EN     0x00000300
#define RA_DNOC_PORT_EN       0x00000304
#define RA_DNOC_PORT_TEST_0   0x00000308
#define RA_DNOC_PORT_TEST_1   0x0000030c
#define RA_DNOC_DECODE_MODE   0x00000310

#define RA_DNOC_ROUTE_LUT_0   0x00000314
#define RA_DNOC_ROUTE_LUT_1   0x00000318
#define RA_DNOC_ROUTE_LUT_2   0x0000031c
#define RA_DNOC_ROUTE_LUT_3   0x00000320

#define RA_DNOC_SEL_FIFO_EN   0x00000334
#define RA_DNOC_STAT_EN       0x00000338
#define RA_DNOC_STAT_MOD_CTRL 0x0000033c
#define RA_DNOC_STAT_TOP_CTRL 0x00000340
#define RA_DNOC_STAT_TOP_ADDR 0x00000344

///////////////////////////////////////////////////////////////////////////////

#define RA_OSPI_CORE_EN       0x000000d0
#define RA_OSPI_FRONTEND      0x000000d4
#define RA_OSPI_TX_BUFFER     0x000000d8
#define RA_OSPI_RX_BUFFER     0x000000dc

#define OSPI_M_SPI      0   // SPI cycle
#define OSPI_M_DSPI     1   // dual SPI cycle
#define OSPI_M_QSPI     2   // quad SPI cycle
#define OSPI_M_OSPI     3   // octal SPI cycle
#define OSPI_M_START2   4   // slave select cycle 2 clocks delay
#define OSPI_M_START1   5   // slave select cycle 1 clock delay
#define OSPI_M_STOP2    6   // slave deselect cycle 2 clocks delay
#define OSPI_M_STOP1    7   // slave deselect cycle 1 clocks delay

#define OSPI_A_D        4   // dummy cycle
#define OSPI_A_DR       6   // dummy read cycle
#define OSPI_A_W        1   // write cycle
#define OSPI_A_R        2   // read cycle
#define OSPI_A_RW       3   // RW cycle

#define OSPI_SS_0       1   // select slave 0
#define OSPI_SS_1       2
#define OSPI_SS_2       4
#define OSPI_SS_3       8

///////////////////////////////////////////////////////////////////////////////
// SPI FLASH
#define CMD_FLASH_READ_ID   0x9F	// get FLASH ID (MT25Q128)

#define CMD_FLASH_READ      0x03    // READ MEMORY, SSPI
#define CMD_FLASH_READ_DSPI 0xBB    // READ MEMORY, DSPI
#define CMD_FLASH_WE        0x06
#define CMD_FLASH_SS_ERASE  0x20
#define CMD_FLASH_FSR_READ  0x70
#define CMD_FLASH_PAGE_PROG 0x02
#define CMD_FLASH_RD_STATUS 0x05


///////////////////////////////////////////////////////////////////////////////
enum SC_TIME {
	SC_MS = 1,
	SC_US = 1000,
	SC_NS = 10000000
};


enum JIB_IF {
	IF_SPI2  = 0,
	IF_UART2 = 1
};

///////////////////////////////////////////////////////////////////////////////

#define SIM_ERROR "\x1B[31mSIM ERROR: \x1B[0m"
#define SIM_INFO "\x1B[32mSIM INFO:  \x1B[0m"



#endif /* JIB1_DEFINES_H_ */
