/*
 * regfile_access_pll.h
 *
 *  Created on: Jul 11, 2019
 *      Author: stolba
 */

#ifndef JIB1_REGFILE_ACCESS_PLL_H_
#define JIB1_REGFILE_ACCESS_PLL_H_

// PLL config vectors
// 500MHz
#define SERIO_PLL_ON_CFG0   0x60198402
#define SERIO_PLL_ON_CFG1   0x0040d960
#define SERIO_PLL_OFF_CFG0  0x20198402
#define SERIO_PLL_OFF_CFG1  0x0040d960

// 1GHz
#define CORE_PLL_ON_CFG0    0x80190021
#define CORE_PLL_ON_CFG1    0x00031040
#define CORE_PLL_OFF_CFG0   0x00190021
#define CORE_PLL_OFF_CFG1   0x00031040

enum pll_id {
    JIB_PLL_OTHER = -1,
    JIB_CORE_0    = 0,
    JIB_CORE_1    = 1,
    JIB_C2C_0     = 2,
    JIB_C2C_1     = 3
};

struct pll_config {
    uint32_t cfg0;
    uint32_t cfg1;
};

uint8_t rf_pll_write (enum pll_id id, uint32_t addr, uint32_t val)
{
    uint32_t addr_rf = 0;

    switch (id) {
        case JIB_CORE_0: addr_rf = 0x20 + addr; break;
        case JIB_CORE_1: addr_rf = 0x40 + addr; break;
        case JIB_C2C_0:
        case JIB_C2C_1:  addr_rf = 0x10 + addr; break;
        default: return 0;
    };

    switch (id) {
        case JIB_CORE_0:
        case JIB_CORE_1: return write_jibcore_rf (addr_rf, val); break;
        case JIB_C2C_0:  return write_c2c_rf (0, addr_rf, val);  break;
        case JIB_C2C_1:  return write_c2c_rf (1, addr_rf, val);  break;
        default: return 0;
    };

    /* wrong id */
    return 0;
}

uint8_t rf_pll_read (enum pll_id id, uint32_t addr, uint32_t *val)
{
    uint32_t addr_rf = 0;

    switch (id) {
        case JIB_CORE_0: addr_rf = 0x20 + addr; break;
        case JIB_CORE_1: addr_rf = 0x40 + addr; break;
        case JIB_C2C_0:
        case JIB_C2C_1:  addr_rf = 0x10 + addr; break;
        default: return 0;
    };

    switch (id) {
        case JIB_CORE_0:
        case JIB_CORE_1: return read_jibcore_rf (addr_rf, val); break;
        case JIB_C2C_0:  return read_c2c_rf (0, addr_rf, val);  break;
        case JIB_C2C_1:  return read_c2c_rf (1, addr_rf, val);  break;
        default: return 0;
    };

    /* wrong id */
    return 0;
}

uint8_t rf_pll_config (enum pll_id id, const struct pll_config config)
{
    uint8_t  result = 1;

    result &= rf_pll_write (id, 0x4, config.cfg1);
    result &= rf_pll_write (id, 0x0, config.cfg0);

    return result;
}

uint8_t rf_pll_status (enum pll_id id, uint32_t *status)
{
    uint8_t  result = 1;

    result &= rf_pll_read (id, 0x8, status);

    return result;
}


uint8_t rf_pll_bisc_config (enum pll_id id, uint32_t config)
{
    uint8_t  result = 1;

    result &= rf_pll_write (id, 0xc, config);

    return result;
}

uint8_t rf_pll_bisc_status (enum pll_id id, uint32_t *status)
{
    uint8_t  result = 1;

    result &= rf_pll_read (id, 0x10, status);

    return result;
}

uint8_t rf_freq_bist_config (uint32_t config, uint32_t select)
{
    uint32_t addr_rf_conf = 0xe0;
    uint32_t addr_rf_sel  = 0x18;
    uint8_t  result = 1;
    result &= write_jibcore_rf (addr_rf_sel,  select);
    result &= write_top_rf (addr_rf_conf, config);

    return result;
}

uint8_t rf_freq_bist_status (uint32_t *status)
{
    uint32_t addr_rf = 0xe4;
    return read_top_rf (addr_rf, status);
}

///////////////////////////////////////////////////////////////////////
uint8_t rf_pll_start_wait_lock (enum pll_id id) {

    uint32_t status;
    for (uint8_t i = 0; i < 20; i++) {
        uint8_t  locked;
        wait (1, SC_US);

        rf_pll_status (id,&status);

        locked = (id == JIB_CORE_1) ? (status & (1 << 20)) : (status & 1<<1);

        if (locked) {
        	printf ("PLL locked after %d us", (int) (i+1));
            return 1;
        }
    }

    printf ("PLL not locked :(");
    return 0;
}
////////////////////////////////////////////////////////////////////////

uint8_t rf_pll_start (enum pll_id id) {
    // startup PLL
    struct pll_config core_pllcfg_on;
    struct pll_config serio_pllcfg_on;
    core_pllcfg_on.cfg0     = CORE_PLL_ON_CFG0;
    core_pllcfg_on.cfg1     = CORE_PLL_ON_CFG1;
    serio_pllcfg_on.cfg0    = SERIO_PLL_ON_CFG0;
    serio_pllcfg_on.cfg1    = SERIO_PLL_ON_CFG1;

    printf ("activating PLL %d ...", (int) id);

    switch(id){
        case JIB_CORE_0:
            rf_pll_config (id, serio_pllcfg_on);
            break;
        case JIB_CORE_1:
            rf_pll_config (id, core_pllcfg_on);
            break;
        case JIB_C2C_0:
            rf_pll_config (id, serio_pllcfg_on);
            break;
        case JIB_C2C_1:
            rf_pll_config (id, serio_pllcfg_on);
            break;
        default: return 0;
    }

    return rf_pll_start_wait_lock(id);
}

////////////////////////////////////////////////////////////////////////






#endif /* JIB1_REGFILE_ACCESS_PLL_H_ */
