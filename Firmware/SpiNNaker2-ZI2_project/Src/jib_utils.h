/*
 * jib_utils.h
 *
 *  Created on: 26 nov. 2019
 *      Author: richi
 */

#include "jib1_defines.h"
#include "spi.h"
#include "gpio.h"

void Jib1_Init();
void crc8_init(uint8_t poly);
uint8_t noc_mmap_write(uint32_t addr, uint32_t data);
uint8_t noc_mmap_read(uint32_t addr, uint32_t *data);
uint8_t SPI1_noc_mmap_write(uint32_t addr, uint32_t data);
uint8_t SPI1_noc_mmap_read(uint32_t addr, uint32_t *data);
uint8_t JSPI_get_status();
uint32_t JSPI_get_32bit();
uint8_t crc8_table[256];
uint8_t crc8_poly;
