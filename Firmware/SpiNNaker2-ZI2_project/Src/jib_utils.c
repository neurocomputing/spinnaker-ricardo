/*
 * jib_utils.c
 *
 *  Created on: 26 nov. 2019
 *      Author: richi
 */

#include "jib_utils.h"
void Jib1_Init() {
// reseting Chips 4+5, no print-out
	/*HAL_GPIO_WritePin(GPIOB, JIB5_G0_Pin, GPIO_PIN_SET);
	 HAL_GPIO_WritePin(GPIOB, JIB5_G1_Pin, GPIO_PIN_RESET);

	 JIB1_3_RESET_WritePin(GPIO_PIN_SET);
	 JIB4_RESET_WritePin(GPIO_PIN_RESET);
	 JIB5_RESET_WritePin(GPIO_PIN_RESET);
	 HAL_Delay(200);
	 JIB1_3_RESET_WritePin(GPIO_PIN_RESET);
	 JIB4_RESET_WritePin(GPIO_PIN_SET);
	 JIB5_RESET_WritePin(GPIO_PIN_SET);*/

// prepare SPI master, execute one access without NSS = 0, to ensure proper cpol
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET); // Deactivate slave

	uint8_t tx_bytes[1] = { 0 };
	uint8_t rx_bytes[1] = { 0 };
	HAL_SPI_TransmitReceive(&hspi1, tx_bytes, rx_bytes, 1, 10);

	crc8_init(0xd5);

}

void crc8_init(uint8_t poly) {
	crc8_poly = poly;
	for (int i_entry = 0; i_entry < (1 << 8); i_entry++) {
		uint8_t state = i_entry;
		for (int i_step = 0; i_step < 8; i_step++) {
			if ((state >> 7) & 1) {
				state = (state << 1) ^ poly;
			} else {
				state <<= 1;
			}
		}
		crc8_table[i_entry] = state;
	}
}

uint8_t noc_mmap_write(uint32_t addr, uint32_t data) {
	return SPI1_noc_mmap_write(addr, data);
}
///////////////////////////////////////////////////////////////////////////////
uint8_t noc_mmap_read(uint32_t addr, uint32_t *data) {
	return SPI1_noc_mmap_read(addr, data);
}

///////////////////////////////////////////////////////////////////////////////
uint8_t SPI1_noc_mmap_write(uint32_t addr, uint32_t data) {
// memory mapped write request 32bit data   // B_RX_CMD, B_RX_A3,...,B_RX_A0, B_RX_D3,...,B_RX_D0
	uint8_t tx_bytes[9] = { CMD_J_WRITE_MMAP_W32, (addr >> 24), (addr >> 16),
			(addr >> 8), (addr >> 0), (data >> 24), (data >> 16), (data >> 8),
			(data >> 0) };
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET); // Activate slave
	if (HAL_SPI_Transmit(&hspi1, tx_bytes, 9, 10) != HAL_OK)
		return 0;
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET); // Deactivate slave
	return 1;
}
///////////////////////////////////////////////////////////////////////////////
uint8_t SPI1_noc_mmap_read(uint32_t addr, uint32_t *data) {
// memory mapped read request  32bit data   // B_RX_CMD, B_RX_A3,...,B_RX_A0
	uint8_t mmap_r_req[5] = { CMD_J_WRITE_MMAP_R32, (addr >> 24), (addr >> 16),
			(addr >> 8), (addr >> 0) };

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET); // Activate slave
	HAL_SPI_Transmit(&hspi1, mmap_r_req, 5, 10);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET); // Deactivate slave

// get status if packet available

	if (JSPI_get_status() & 0x1) {
		*data = JSPI_get_32bit();
	}

	return 1;
}

uint8_t JSPI_get_status() {
// get SPI slave interface status
	uint8_t tx_bytes[2] = { CMD_J_STATUS, 0x0 };
	uint8_t rx_bytes[2] = { 0, 0 };

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET); // Activate slave
	HAL_SPI_TransmitReceive(&hspi1, tx_bytes, rx_bytes, 2, 10);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET); // Deactivate slave
	return rx_bytes[1];
}
///////////////////////////////////////////////////////////////////////////////
uint32_t JSPI_get_32bit() {
// read only data 32bit data                // B_RX_CMD, B_TX_D3,...,B_TX_D0
	uint8_t rd_buf[5] = { CMD_J_READ_D_32, 0, 0, 0, 0 };
	uint8_t rx_bytes[5];
	uint32_t ret_val = 0;
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET); // Activate slave
	if (HAL_SPI_TransmitReceive(&hspi1, rd_buf, rx_bytes, 5, 10) != HAL_OK)
		return 0;
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET); // Deactivate slave

	ret_val = (rx_bytes[1] << 24) | (rx_bytes[2] << 16) | (rx_bytes[3] << 8)
			| (rx_bytes[4] << 0);

	return ret_val;
}
