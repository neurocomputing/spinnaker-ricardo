/*
 * gpio.h
 *
 *  Created on: 26 nov. 2019
 *      Author: richi
 */

#include "stm32h7xx_hal.h"


#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define LD1_Pin GPIO_PIN_0
#define LD1_GPIO_Port GPIOB
#define LD3_Pin GPIO_PIN_14
#define LD3_GPIO_Port GPIOB
#define USB_OTG_FS_PWR_EN_Pin GPIO_PIN_10
#define USB_OTG_FS_PWR_EN_GPIO_Port GPIOD
#define USB_OTG_FS_OVCR_Pin GPIO_PIN_7
#define USB_OTG_FS_OVCR_GPIO_Port GPIOG
#define LD2_Pin GPIO_PIN_1
#define LD2_GPIO_Port GPIOE

PCD_HandleTypeDef hpcd_USB_OTG_FS;


void MX_GPIO_Init(void);
void MX_USB_OTG_FS_PCD_Init(void);
